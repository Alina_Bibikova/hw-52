import React, {Component} from 'react';
import NewNumber from './Numbers/Numbers';

import './App.css';

class App extends Component {

    state = {
        numbers: []
    };

    newNumbers = () => {
        let numbers = [];

        while (numbers.length < 5) {
            let randomNumber = Math.floor(Math.random() * 32) + 5;
            if (!numbers.includes(randomNumber)) numbers.push(randomNumber);
        }

        numbers = numbers.sort((a, b) => a - b);

        this.setState({numbers})
    };

    buttonHandler = () => {
        this.newNumbers();
    };

    render() {
        return (
            <div className="container">
                <div>
                    <h1 className='thisLotto'>This is Bingo!</h1>
                    <button className='newNumbers' onClick={this.buttonHandler}>New Numbers</button>
                </div>
                <NewNumber numbers={this.state.numbers}/>
            </div>
        );
    }
}

export default App;
