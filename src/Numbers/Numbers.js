import React from "react";

const NewNumber = (props) => {
    return (
        props.numbers.map((number, index) => {
            return (
                <div key={index} className='newNumbersLotto'>
                    <span className="number">{number}</span>
                </div>
            )
        })
    );
};

export default NewNumber;